# Copyright 2015 Cisco Systems, Inc.
# All rights reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import logging

from yacore import version
from yacore import log
log.early_init_log(logging.DEBUG)

from yacore.config import com_cli_opts
from yacore.api.config import api_cli_opts
from yacore.db.config import register_options

from oslo_config import cfg

CONF = cfg.CONF

LOG = logging.getLogger(__name__)


def cli_opts_register():
    cfg.CONF.register_cli_opts(com_cli_opts)
    cfg.CONF.register_cli_opts(api_cli_opts, group='rest')
    register_options()


def prepare_service(args=None):

    cli_opts_register()
    try:
        CONF(args=args, project='yacore', version=version,
             default_config_files=['/etc/yacore/yacore.ini'])
    except cfg.ConfigFilesNotFoundError:
        CONF(args=args, project='yacore', version=version)

    log.init_log()
    LOG.info('Log (Re)opened.')
