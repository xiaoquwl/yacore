# Copyright 2015 Cisco Systems, Inc.
# All rights reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

"""API return code
"""

CODE_DICT = {
    # POST data format error
    0x000: 'POST data format error',
    # database
    0x001: 'Database Error',
    # route policy
    0x101: 'Policy does not exist',  # 257
    0x102: 'Prefix already managed by policy',  # 258
    0x103: 'Can not edit(delete) policy which is activate',  # 259
    0x104: 'Policy id must be a 12-byte input or a 24-character hex string'  # 260
}
