# Copyright 2015 Cisco Systems, Inc.
# All rights reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

""" api version 2"""

import logging
import traceback

import flask
from flask.ext.httpauth import HTTPBasicAuth
from oslo_config import cfg
from bson.objectid import ObjectId

from yacore.api import utils as api_utils
from yacore.db import utils as db_utils
from yacore.db import constants as db_cons
from yacore.agent import utils as agent_utils
from yacore.api.code import CODE_DICT

blueprint = flask.Blueprint('v2', __name__)

LOG = logging.getLogger(__name__)
auth = HTTPBasicAuth()


@auth.get_password
def get_pw(username):
    if username == cfg.CONF.rest.username:
        return cfg.CONF.rest.password
    return None


@blueprint.route('/', methods=['GET'])
@auth.login_required
@api_utils.log_request
def root():
    """
    v2 api root. Get the api status.
    """
    intro = {
        "status": "stable",
        "updated": "2015-10-22T00:00:00Z",
        "version": "v2"}
    return flask.jsonify(intro)


@blueprint.route('/prefix/search/', methods=['GET'])
@auth.login_required
@api_utils.log_request
def search_prefix():
    """
    search prefixes from source yabgp agents.

    **Example request**:

    .. sourcecode:: http

      GET /v2/prefix/search HTTP/1.1
      Host: example.com
      Accept: application/json

    **Example response**:

    .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/json
        {
          "1.1.1.1/32": {
            "10.124.1.221": [
              {
                "bind_host": "10.75.44.10",
                "bind_port": 8801
              },
              {
                "bind_host": "10.75.44.11",
                "bind_port": 8801
              }
            ]
          }
          }

    :status 200: the api can work.
    """
    # community = flask.request.args.get('community')
    source = flask.request.args.get('source')

    # get all soucre yabgp agents from database
    all_source_agents = db_utils.get_yabgp_agent_from_db(
        query={'tag': {'$in': [db_cons.SOURCE_AND_TARGET_ROUTER_TAG, db_cons.SOURCE_ROUTER_TAG]}})
    if not all_source_agents:
        return flask.abort(500)

    # try to search prefix from rest api
    results = {}
    for agent in all_source_agents:
        if source and agent['peer_ip'] in source or source is None:
            prefix_list = agent_utils.search_prefix_from_agent_rib_in(
                bind_host=agent['bind_host'],
                bind_port=agent['bind_port'],
                peer_ip=agent['peer_ip']
            )
            print prefix_list
            if prefix_list:
                for prefix in prefix_list['prefixes']:
                    if prefix not in results:
                        results[prefix] = {
                            agent['peer_ip']: [{
                                'bind_host': agent['bind_host'],
                                'bind_port': agent['bind_port']
                            }]}
                    else:
                        if agent['peer_ip'] not in results[prefix]:
                            results[prefix][agent['peer_ip']] = [{
                                'bind_host': agent['bind_host'],
                                'bind_port': agent['bind_port']
                            }]
                        else:
                            results[prefix][agent['peer_ip']].append({
                                'bind_host': agent['bind_host'],
                                'bind_port': agent['bind_port']
                            })
        else:
            continue
    return flask.jsonify(results)


@blueprint.route('/prefix/attribute/', methods=['GET'])
@auth.login_required
@api_utils.log_request
def get_prefix_attribute():
    """
    get prefix's attribute.

    **Example request**:

    .. sourcecode:: http

      GET /v2/prefix/attribute?prefix=1.1.1.1/32&peer_ip=10.124.1.221
      &bind_host=10.10.10.10&bind_port=8801 HTTP/1.1
      Host: example.com
      Accept: application/json

    **Example response**:

    .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/json
      {
        "attributes": [
            "1.1.1.1/32",
            "2.2.2.2/32"
        ]
      }

    :status 200: the api can work.
    """
    prefix = flask.request.args.get('prefix')
    peer_ip = flask.request.args.get('peer_ip')
    bind_host = flask.request.args.get('bind_host')
    bind_port = flask.request.args.get('bind_port')

    return flask.jsonify(agent_utils.get_prefix_attr_rib_in(
        bind_host=bind_host, bind_port=bind_port, peer_ip=peer_ip, prefix=prefix))


@blueprint.route('/policy', methods=['POST'])
@auth.login_required
@api_utils.log_request
def policy_add():
    """
    Create a new policy and save it into database
    """

    # save policy to database
    LOG.info('Try to save route policy to database')
    policy_json = flask.request.get_json()
    try:
        result = db_utils.manage_route_policy(policy_json)
        policy_id = str(result)
    except Exception as e:
        LOG.error(e)
        LOG.debug(traceback.format_exc())
        LOG.info(CODE_DICT[0x101])
        return flask.jsonify({
            'status': False,
            'code': 0x001
        })
    # policy status check
    if not policy_json.get('activate'):
        # just save the policy and return
        return flask.jsonify({
            'status': True,
            'policy_id': policy_id
        })

    LOG.info('Try to activate this policy')
    activate_policy(policy_json.get('filter'), policy_json.get('apply_to'))
    return flask.jsonify({
        'status': True,
        'policy_id': policy_id
    })


def activate_policy(policy_filter, apply_to_peer):

    # update database and agent apply to
    filter_list = db_utils.manage_channel_filter(filters=policy_filter, peer_ip=apply_to_peer)
    # get all soucre yabgp agents from database
    LOG.info('update filter to apply peer agent')
    source_agents = db_utils.get_yabgp_agent_from_db(
        query={
            'tag': {'$in': [db_cons.SOURCE_AND_TARGET_ROUTER_TAG, db_cons.SOURCE_ROUTER_TAG]},
            'peer_ip': apply_to_peer})
    for agent in source_agents:
        agent_utils.update_channel_filter(
            bind_host=agent['bind_host'],
            bind_port=agent['bind_port'],
            filter_dict={'filter': filter_list}
            )
        agent_utils.send_route_refresh_to_peer(
            bind_host=agent['bind_host'],
            bind_port=agent['bind_port'],
            peer_ip=agent['peer_ip']
        )


def deactivate_policy(policy_id):
    LOG.info('Try to withdraw prefix sent by this policy')
    activate_prefix_list = db_utils.manage_activate_prefix(query={'policy_id': policy_id})
    for activate_prefix in activate_prefix_list:
        # TODO check channel filter and update them
        # delete prefix from database
        LOG.info('try to withdraw this activate prefix %s from dababase', activate_prefix['prefix_send'])
        db_utils.manage_activate_prefix(
            query={'policy_id': policy_id, 'prefix_send': activate_prefix['prefix_send']}, delete=True)
        # try to send withdraw to dst peer
        for dst_peer in activate_prefix['dst_peer']:
            all_agents = db_utils.get_yabgp_agent_from_db(query={'peer_ip': dst_peer})
            for _agent in all_agents:
                LOG.info('try to withdraw prefix %s from agent %s:%s which connected peer %s',
                         activate_prefix['prefix_send'], _agent['bind_host'], _agent['bind_port'], _agent['peer_ip'])
                agent_utils.send_update_to_peer(
                    bind_host=_agent['bind_host'],
                    bind_port=_agent['bind_port'],
                    peer_ip=_agent['peer_ip'],
                    data={
                        'withdraw': [activate_prefix['prefix_send']],
                        'nlri': []
                    }
                )


@blueprint.route('/policy/<policy_id>', methods=['PUT'])
@auth.login_required
@api_utils.log_request
def policy_edit(policy_id):
    """
    policy edit option

    :param policy_id:
    :return:
    """
    policy_id = ObjectId(policy_id)
    # for policy does not exist
    if not db_utils.manage_route_policy(query={'_id': policy_id}, count=True):
        LOG.info(CODE_DICT[0x101])
        return flask.jsonify({
            'status': False,
            'code': 0x101
        })

    policy_options = flask.request.get_json()

    # if is activate or deactivate
    if 'activate' in policy_options:
        policy_items = db_utils.manage_route_policy(query={'_id': policy_id})
        if policy_options['activate']:
            activate_policy(policy_items['filter'], policy_items['apply_to'])
            LOG.info('try to update database')
            db_utils.manage_route_policy(policy_dict={'activate': True}, _id=policy_id)
        else:
            deactivate_policy(policy_id)
            LOG.info('try to update database')
            db_utils.manage_route_policy(policy_dict={'activate': False}, _id=policy_id)
        return flask.jsonify({
            'status': True,
            'policy_id': str(policy_id)
        })

    # edit policy options, make sure the policy activate is False
    # for policy is activate
    if db_utils.manage_route_policy(query={'_id': policy_id})['activate']:
        LOG.info(CODE_DICT[0x103])
        return flask.jsonify({
            'status': False,
            'code': 0x103
        })
    return flask.jsonify({
        'status': True,
        'policy_id': str(db_utils.manage_route_policy(policy_dict=policy_options, _id=policy_id))
    })


@blueprint.route('/policy/<policy_id>', methods=['DELETE'])
@auth.login_required
@api_utils.log_request
def policy_delete(policy_id):
    """
    delete policy according the policy id, and make sure that the policy's activate is False

    :param policy_id:
    :return:
    """
    try:
        policy_id = ObjectId(policy_id)
    except Exception as e:
        LOG.error(e)
        LOG.info(CODE_DICT[0x104])
        return flask.jsonify({
            'status': False,
            'code': 0x104
        })
    # for policy does not exist
    if not db_utils.manage_route_policy(query={'_id': policy_id}, count=True):
        LOG.info(CODE_DICT[0x101])
        return flask.jsonify({
            'status': False,
            'code': 0x101
        })
    # for policy is activate
    if db_utils.manage_route_policy(query={'_id': policy_id})['activate']:
        LOG.info(CODE_DICT[0x103])
        return flask.jsonify({
            'status': False,
            'code': 0x103
        })

    # try to delete policy
    LOG.info('try to delete route policy id=%s', policy_id)
    db_utils.manage_route_policy(query={'_id': policy_id}, delete=True)
    return flask.jsonify({
        'status': True
    })


@blueprint.route('/send/update/<peer_ip>', methods=['POST'])
@auth.login_required
@api_utils.log_request
def send_update(peer_ip):

    # check if one of these prefix has already managed by other policies
    afi_safi = 'ipv4'
    post_data = flask.request.get_json()
    if post_data.get('nlri'):
        # for IPv4 update
        if not post_data.get('attr'):
            return flask.jsonify({
                'status': False,
                'code': 0x000
            })
    else:
        if not post_data.get('withdraw'):
            attr = post_data.get('attr')
            # check if this is none ipv4
            if not attr:
                return flask.jsonify({
                    'status': False,
                    'code': 0x000,
                })

            elif '14' in attr or '15' in attr:
                afi_safi = 'flowspec'
            else:
                return flask.jsonify({
                    'status': False,
                    'code': 0x000,
                })

    db_record_template = {
        "prefix_send": None,
        "activate": True,
        "src_agent": [],
        "new_attr": {},
        "src_peer": "",
        "prefix_origin": "",
        "old_attr": {},
        "dst_peer": [
            peer_ip
        ],
        "policy_id": None,
        "afi_safi": afi_safi
    }
    # query = {'dst_peer': {'$in': [peer_ip]}, 'prefix_send': None, 'policy_id': {'$ne': None}}
    # for ipv4
    if afi_safi == 'ipv4':
        for prefix in post_data['nlri']:
            # query['prefix_send'] = prefix
            # if db_utils.manage_activate_prefix(query=query, find_one=True):
            #     return flask.jsonify({
            #         'status': False,
            #         'code': 0x102
            #     })
            # try to save prefix records to db
            # exist? upsert
            db_record_template['prefix_send'] = prefix
            db_record_template['new_attr'] = post_data['attr']
            db_utils.manage_activate_prefix(key={
                'policy_id': None,
                'prefix_send': prefix,
                'dst_peer': [peer_ip]
            }, prefix_dict=db_record_template)

        for prefix in post_data['withdraw']:
            # query['prefix_send'] = prefix
            # if db_utils.manage_activate_prefix(query=query, find_one=True):
            #     return flask.jsonify({
            #         'status': False,
            #         'code': 0x102
            #     })
            # try to delete
            db_utils.manage_activate_prefix(
                query={
                    'prefix_send': prefix,
                    'policy_id': None,
                    'dst_peer': [peer_ip]
                }, delete=True)
    else:
        if '14' in post_data['attr']:
            for prefix in post_data['attr']['14']['nlri']:
                db_record_template['prefix_send'] = prefix
                db_record_template['new_attr'] = post_data['attr']
                db_utils.manage_activate_prefix(key={
                    'policy_id': None,
                    'prefix_send': prefix,
                    'dst_peer': [peer_ip]
                }, prefix_dict=db_record_template)
        elif '15' in post_data['attr']:
            for prefix in post_data['attr']['15']['withdraw']:
                db_utils.manage_activate_prefix(
                    query={
                        'prefix_send': prefix,
                        'policy_id': None,
                        'dst_peer': [peer_ip]
                    }, delete=True)
    # get all agents connected to this peer
    all_agents = db_utils.get_yabgp_agent_from_db(query={'peer_ip': peer_ip})
    for _agent in all_agents:
        agent_utils.send_update_to_peer(
            bind_host=_agent['bind_host'],
            bind_port=_agent['bind_port'],
            peer_ip=_agent['peer_ip'],
            data=post_data
        )
    return flask.jsonify({
        'status': True
    })


@blueprint.route('/peers', methods=['GET'])
@auth.login_required
@api_utils.log_request
def get_peer_list():
    try:
        results = db_utils.get_yabgp_agent_from_db()
        # try to get each agent's running information
        agent_list = []
        for agent in results:
            agent_state = agent_utils.get_agent_state(
                bind_host=agent['bind_host'],
                bind_port=agent['bind_port'],
                peer_ip=agent['peer_ip']
            )
            if not agent_state:
                agent['fsm'] = 'unknow'
                agent['uptime'] = 'unknow'
            elif isinstance(agent_state, dict):
                if agent_state['peer']:
                    agent['fsm'] = agent_state['peer']['fsm']
                    agent['uptime'] = agent_state['peer']['uptime']
                else:
                    agent['fsm'] = 'unknow'
                    agent['uptime'] = 'unknow'
            agent_list.append(agent)
        return flask.jsonify(
            {
                'peers': agent_list,
                'status': True
            })
    except Exception as e:
        LOG.error(e)
        LOG.debug(traceback.format_exc())
        return flask.jsonify({
            'status': False,
            'code': 0x001
        })
