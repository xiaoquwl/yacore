# Copyright 2015 Cisco Systems, Inc.
# All rights reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

""" api version 1"""

import logging
import traceback

import flask
from flask.ext.httpauth import HTTPBasicAuth
from oslo_config import cfg
from bson.objectid import ObjectId

from yacore.api import utils as api_utils
from yacore.db import utils as db_utils
from yacore.db import constants as db_cons
from yacore.agent import utils as agent_utils

blueprint = flask.Blueprint('v1', __name__)

LOG = logging.getLogger(__name__)
auth = HTTPBasicAuth()


@auth.get_password
def get_pw(username):
    if username == cfg.CONF.rest.username:
        return cfg.CONF.rest.password
    return None


@blueprint.route('/', methods=['GET'])
@auth.login_required
@api_utils.log_request
def root():
    """
    v1 api root. Get the api status.

    **Example request**:

    .. sourcecode:: http

      GET /v1 HTTP/1.1
      Host: example.com
      Accept: application/json

    **Example response**:

    .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/json
      {
        "status": "stable",
        "updated": "2015-01-22T00:00:00Z",
        "version": "v1"
      }

    :status 200: the api can work.
    """
    intro = {
        "status": "stable",
        "updated": "2015-07-22T00:00:00Z",
        "version": "v1"}
    return flask.jsonify(intro)


@blueprint.route('/prefix/search/', methods=['GET'])
@auth.login_required
@api_utils.log_request
def search_prefix():
    """
    search prefixes from source yabgp agents.

    **Example request**:

    .. sourcecode:: http

      GET /v1/prefix/search HTTP/1.1
      Host: example.com
      Accept: application/json

    **Example response**:

    .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/json
        {
          "1.1.1.1/32": {
            "10.124.1.221": [
              {
                "bind_host": "10.75.44.10",
                "bind_port": 8801
              },
              {
                "bind_host": "10.75.44.11",
                "bind_port": 8801
              }
            ]
          }
          }

    :status 200: the api can work.
    """
    # community = flask.request.args.get('community')
    source = flask.request.args.get('source')

    # get all soucre yabgp agents from database
    all_source_agents = db_utils.get_yabgp_agent_from_db(
        query={'tag': {'$in': [db_cons.SOURCE_AND_TARGET_ROUTER_TAG, db_cons.SOURCE_ROUTER_TAG]}})
    if not all_source_agents:
        return flask.abort(500)

    # try to search prefix from rest api
    results = {}
    for agent in all_source_agents:
        if source and agent['peer_ip'] in source or source is None:
            prefix_list = agent_utils.search_prefix_from_agent_rib_in(
                bind_host=agent['bind_host'],
                bind_port=agent['bind_port'],
                peer_ip=agent['peer_ip']
            )
            if prefix_list:
                for prefix in prefix_list['prefixes']:
                    if prefix not in results:
                        results[prefix] = {
                            agent['peer_ip']: [{
                                'bind_host': agent['bind_host'],
                                'bind_port': agent['bind_port']
                            }]}
                    else:
                        if agent['peer_ip'] not in results[prefix]:
                            results[prefix][agent['peer_ip']] = [{
                                'bind_host': agent['bind_host'],
                                'bind_port': agent['bind_port']
                            }]
                        else:
                            results[prefix][agent['peer_ip']].append({
                                'bind_host': agent['bind_host'],
                                'bind_port': agent['bind_port']
                            })
        else:
            continue
    return flask.jsonify(results)


@blueprint.route('/prefix/attribute/', methods=['GET'])
@auth.login_required
@api_utils.log_request
def get_prefix_attribute():
    """
    get prefix's attribute.

    **Example request**:

    .. sourcecode:: http

      GET /v1/prefix/attribute?prefix=1.1.1.1/32&peer_ip=10.124.1.221
      &bind_host=10.10.10.10&bind_port=8801 HTTP/1.1
      Host: example.com
      Accept: application/json

    **Example response**:

    .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/json
      {
        "attributes": [
            "1.1.1.1/32",
            "2.2.2.2/32"
        ]
      }

    :status 200: the api can work.
    """
    prefix = flask.request.args.get('prefix')
    peer_ip = flask.request.args.get('peer_ip')
    bind_host = flask.request.args.get('bind_host')
    bind_port = flask.request.args.get('bind_port')

    return flask.jsonify(agent_utils.get_prefix_attr_rib_in(
        bind_host=bind_host, bind_port=bind_port, peer_ip=peer_ip, prefix=prefix))


@blueprint.route('/policy/add/', methods=['POST'])
@auth.login_required
@api_utils.log_request
def policy_send():
    """
    {
        'activate': true
        'apply_to': '10.124.1.221',
        'filter': {
            # 'prefix': '1.1.1.1/32'
            'attr': {
                'opt': 'and',
                'value': [
                    {
                        'type': 'as_path'
                        'value': 4837},
                    {
                        'type': 'community'
                        'value': ['4837:1239']
                    }
                ]
            }
        },
        'priority': 0
        'action': {
            'send_to': ['10.124.1.245'],
            'attr': {
                'next_hop': {
                    'value': '1.1.1.1',
                    'opt': 'overwrite'
                },
                'local_preference': {
                    'value': 500,
                    'opt': 'overwrite'
                }
                'community': {
                    'value': 'NO_EXPORT',
                    'opt': 'append'
                }
            }
        }
    }
    :return:
    """
    # save policy to database
    LOG.info('save route policy to database')
    policy_json = flask.request.get_json()
    try:
        result = db_utils.manage_route_policy(policy_json)
        policy_id = str(result)
    except Exception as e:
        LOG.error(e)
        LOG.debug(traceback.format_exc())
        return flask.jsonify({
            'status': False,
            'code': 0x001
        })

    # get filter from policy
    LOG.info('update filter in database')
    policy_filter = policy_json.get('filter')
    apply_to_peer = policy_json.get('apply_to')

    # update database and agent apply to
    filter_list = db_utils.manage_channel_filter(filters=policy_filter, peer_ip=apply_to_peer)
    # get all soucre yabgp agents from database
    LOG.info('update filter to apply peer agent')
    source_agents = db_utils.get_yabgp_agent_from_db(
        query={
            'tag': {'$in': [db_cons.SOURCE_AND_TARGET_ROUTER_TAG, db_cons.SOURCE_ROUTER_TAG]},
            'peer_ip': apply_to_peer})
    for agent in source_agents:
        agent_utils.update_channel_filter(
            bind_host=agent['bind_host'],
            bind_port=agent['bind_port'],
            filter_dict={'filter': filter_list}
            )
        # action or not
        if policy_json.get('activate') is True:
            LOG.info('try to activate policy now')
            agent_utils.send_route_refresh_to_peer(
                bind_host=agent['bind_host'],
                bind_port=agent['bind_port'],
                peer_ip=agent['peer_ip']
            )
    return flask.jsonify({
        'status': True,
        'policy_id': policy_id
    })


@blueprint.route('/policy/delete/<policy_id>', methods=['GET'])
@auth.login_required
@api_utils.log_request
def policy_delete(policy_id):
    """
    delete policy according the policy id
    :param policy_id:
    :return:
    """
    policy_id = ObjectId(policy_id)
    if not db_utils.manage_route_policy(query={'_id': policy_id}, count=True):
        return flask.jsonify({
            'status': False,
            'code': 0x101
        })

    # try to delete policy
    LOG.info('try to delete route policy id=%s', policy_id)
    db_utils.manage_route_policy(query={'_id': policy_id}, delete=True)
    activate_prefix_list = db_utils.manage_activate_prefix(query={'policy_id': policy_id})
    for activate_prefix in activate_prefix_list:
        # delete and send withdraw
        # {
        #     "_id" : ObjectId("55fb57f51b9533256f519941"),
        #     "activate" : true,
        #     "src_agent" : [
        #         "10.75.44.11:8801",
        #         "10.75.44.10:8801"
        #     ],
        #     "new_attr" : {},
        #     "src_peer" : "10.124.1.245",
        #     "prefix" : "2.2.2.2/32",
        #     "old_attr" : {},
        #     "dst_peer" : [
        #         "10.124.1.242",
        #         "10.124.1.251"
        #     ],
        #     "policy_id" : ObjectId("55fa7538b1993fdf8a051765")
        # }
        # TODO check channel filter and update them
        # delete prefix from database
        LOG.info('try to withdraw this activate prefix %s from dababase', activate_prefix['prefix_send'])
        db_utils.manage_activate_prefix(
            query={'policy_id': policy_id, 'prefix_send': activate_prefix['prefix_send']}, delete=True)
        # try to send withdraw to dst peer
        for dst_peer in activate_prefix['dst_peer']:
            all_agents = db_utils.get_yabgp_agent_from_db(query={'peer_ip': dst_peer})
            for _agent in all_agents:
                LOG.info('try to withdraw prefix %s from agent %s:%s which connected peer %s',
                         activate_prefix['prefix_send'], _agent['bind_host'], _agent['bind_port'], _agent['peer_ip'])
                agent_utils.send_update_to_peer(
                    bind_host=_agent['bind_host'],
                    bind_port=_agent['bind_port'],
                    peer_ip=_agent['peer_ip'],
                    data={
                        'withdraw': [activate_prefix['prefix_send']],
                        'nlri': []
                    }
                )
    return flask.jsonify({
        'status': True
    })


@blueprint.route('/send/update/<peer_ip>', methods=['POST'])
@auth.login_required
@api_utils.log_request
def send_update(peer_ip):
    """
    try to send update to the peer and save results to database
    :param peer_ip:
    :return:

    POST data example:
        # update
        {
            "attr":{
                "1": 0,
                "2": [],
                "3": "192.0.2.1",
                "5": 100,
                "8": ["NO_EXPORT"]
        },
            "nlri": ["172.20.1.0/24", "172.20.2.0/24"]
        }
        # withdraw
        {
            "withdraw": ["172.20.1.0/24", "172.20.2.0/24"]
        }

    Database structure:
    {
        "prefix_send" : "180.11.8.0/21",
        "activate" : true,
        "src_agent" : []
        "new_attr" : {
            "1" : 0,
            "3" : "10.1.115.5",
            "2" : [
                [
                    2,
                    [
                        4134,
                        2000
                    ]
                ]
            ],
            "5" : 600,
            "4" : 0,
            "8" : [
                "4134:200"
            ]
        },
        "src_peer" : "",
        "prefix_origin" : "",
        "old_attr" : {},
        "dst_peer" : [
            "10.75.44.209"
        ],
        "policy_id" : None
    }
    """
    # check if one of these prefix has already managed by other policies
    afi_safi = 'ipv4'
    post_data = flask.request.get_json()
    if post_data.get('nlri'):
        # for IPv4 update
        if not post_data.get('attr'):
            return flask.jsonify({
                'status': False,
                'code': 0x000
            })
    else:
        if not post_data.get('withdraw'):
            attr = post_data.get('attr')
            # check if this is none ipv4
            if not attr:
                return flask.jsonify({
                    'status': False,
                    'code': 0x000,
                })

            elif '14' in attr or '15' in attr:
                afi_safi = 'flowspec'
            else:
                return flask.jsonify({
                    'status': False,
                    'code': 0x000,
                })

    db_record_template = {
        "prefix_send": None,
        "activate": True,
        "src_agent": [],
        "new_attr": {},
        "src_peer": "",
        "prefix_origin": "",
        "old_attr": {},
        "dst_peer": [
            peer_ip
        ],
        "policy_id": None,
        "afi_safi": afi_safi
    }
    # query = {'dst_peer': {'$in': [peer_ip]}, 'prefix_send': None, 'policy_id': {'$ne': None}}
    # for ipv4
    if afi_safi == 'ipv4':
        for prefix in post_data['nlri']:
            # query['prefix_send'] = prefix
            # if db_utils.manage_activate_prefix(query=query, find_one=True):
            #     return flask.jsonify({
            #         'status': False,
            #         'code': 0x102
            #     })
            # try to save prefix records to db
            # exist? upsert
            db_record_template['prefix_send'] = prefix
            db_record_template['new_attr'] = post_data['attr']
            db_utils.manage_activate_prefix(key={
                'policy_id': None,
                'prefix_send': prefix,
                'dst_peer': [peer_ip]
            }, prefix_dict=db_record_template)

        for prefix in post_data['withdraw']:
            # query['prefix_send'] = prefix
            # if db_utils.manage_activate_prefix(query=query, find_one=True):
            #     return flask.jsonify({
            #         'status': False,
            #         'code': 0x102
            #     })
            # try to delete
            db_utils.manage_activate_prefix(
                query={
                    'prefix_send': prefix,
                    'policy_id': None,
                    'dst_peer': [peer_ip]
                }, delete=True)
    else:
        if '14' in post_data['attr']:
            for prefix in post_data['attr']['14']['nlri']:
                db_record_template['prefix_send'] = prefix
                db_record_template['new_attr'] = post_data['attr']
                db_utils.manage_activate_prefix(key={
                    'policy_id': None,
                    'prefix_send': prefix,
                    'dst_peer': [peer_ip]
                }, prefix_dict=db_record_template)
        elif '15' in post_data['attr']:
            for prefix in post_data['attr']['15']['withdraw']:
                db_utils.manage_activate_prefix(
                    query={
                        'prefix_send': prefix,
                        'policy_id': None,
                        'dst_peer': [peer_ip]
                    }, delete=True)
    # get all agents connected to this peer
    all_agents = db_utils.get_yabgp_agent_from_db(query={'peer_ip': peer_ip})
    for _agent in all_agents:
        agent_utils.send_update_to_peer(
            bind_host=_agent['bind_host'],
            bind_port=_agent['bind_port'],
            peer_ip=_agent['peer_ip'],
            data=post_data
        )
    return flask.jsonify({
        'status': True
    })
