# Copyright 2015 Cisco Systems, Inc.
# All rights reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import logging
import os
from wsgiref import simple_server

from oslo_config import cfg
import flask

from yacore.api import v1
from yacore.api import v2

app = flask.Flask('yacore.api')
app.config['SECRET_KEY'] = 'cisco123'
app.register_blueprint(v1.blueprint, url_prefix='/v1')
app.register_blueprint(v2.blueprint, url_prefix='/v2')

LOG = logging.getLogger(__name__)


def build_server():

    LOG.info("Configuration:")
    cfg.CONF.log_opt_values(LOG, logging.INFO)
    LOG.info('Starting build WSGI server')
    # Create the WSGI server and start it
    host, port = cfg.CONF.rest.bind_host, cfg.CONF.rest.bind_port
    server_cls = simple_server.WSGIServer

    srv = simple_server.make_server(host, port, app,
                                    server_cls)

    LOG.info('Starting server in PID %s' % os.getpid())

    # write pid file
    if cfg.CONF.pid_file:
        with open(cfg.CONF.pid_file, 'w') as pid_file:
            pid_file.write(str(os.getpid()))
            LOG.info('create pid file: %s' % cfg.CONF.pid_file)

    if host == '0.0.0.0':
        LOG.info(
            'serving on 0.0.0.0:%(sport)s, view at http://127.0.0.1:%(vport)s'
            % ({'sport': port, 'vport': port}))
    else:
        LOG.info("serving on http://%(host)s:%(port)s" % (
                 {'host': host, 'port': port}))
        LOG.info('Register controller url to database')
    return srv
