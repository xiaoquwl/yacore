# Copyright 2015 Cisco Systems, Inc.
# All rights reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

"""all interaction with mongodb database
"""

import logging
import contextlib

from oslo_config import cfg

from yacore.db import constants as db_cons
from yacore.db.mongodb import MongoApi

LOG = logging.getLogger(__name__)
CONF = cfg.CONF

FILTER_TYPE_PREFIX = 'prefix'
FILTER_TYPE_COMMUNITY = 'community'
FILTER_TYPE_AS_PATH = 'as_path'


@contextlib.contextmanager
def mongo_operation(mongo_conn, connection_name):
    mongo_conn.collection_name = connection_name
    db = mongo_conn.get_collection()
    yield db
    mongo_conn._close_db()


def init_mongo_api():

    if CONF.database.use_replica:
        mongo_connection = MongoApi(
            connection_url=CONF.database.connection,
            db_name=CONF.database.dbname,
            use_replica=CONF.database.use_replica,
            replica_name=CONF.database.replica_name,
            read_preference=CONF.database.read_preference,
            write_concern=CONF.database.write_concern,
            w_timeout=CONF.database.write_concern_timeout
        )
    else:
        mongo_connection = MongoApi(connection_url=CONF.database.connection, db_name=CONF.database.dbname)

    return mongo_connection


def get_yabgp_agent_from_db(query=None):
    with mongo_operation(init_mongo_api(), db_cons.MONGO_COLLECTION_BGP_AGENT) as mongo_col:
        if not query:
            return mongo_col.find()
        else:
            return mongo_col.find(query)


def manage_route_policy(policy_dict=None, query=None, delete=False, count=False, _id=None):
    with mongo_operation(init_mongo_api(), db_cons.MONGO_COLLECTION_ROUTE_POLICY) as mongo_col:
        # delete
        if delete:
            mongo_col.remove(query)
            return
        # count
        elif query and count:
            return mongo_col.count(query)
        # find one
        elif query:
            return mongo_col.find_one(query)
        # save or update
        if not _id:
            key = {
                'filter': policy_dict['filter'],
                'apply_to': policy_dict['apply_to'],
                'action.send_to': policy_dict['action']['send_to']
            }
            result = mongo_col.update_one(key, {'$set': policy_dict}, upsert=True)
            if result.upserted_id:
                return result.upserted_id
            else:
                return mongo_col.find_one(key)['_id']
        else:
            mongo_col.update_one({'_id': _id}, {'$set': policy_dict}, upsert=True)
            return _id


def update_channel_filter_in_db(filter_dict, delete=False):
    with mongo_operation(init_mongo_api(), db_cons.MONGO_COLLECTION_RABBIT_CHANNEL_FILTER) as mongo_col:
        if delete:
            return mongo_col.delete(filter_dict)
        else:
            return mongo_col.update_one(filter_dict, {'$set': filter_dict}, upsert=True)


def manage_activate_prefix(query=None, delete=False, find_one=False, count=False, key={}, prefix_dict={}):
    with mongo_operation(init_mongo_api(), db_cons.MONGO_COLLECTION_PREFIX) as mongo_col:
        if query and not delete:
            if not find_one:
                return mongo_col.find(query)
            else:
                return mongo_col.find_one(query)
        elif query and delete:
            return mongo_col.remove(query)
        elif query and count:
            return mongo_col.count(query)
        mongo_col.update_one(key, {'$set': prefix_dict}, upsert=True)
        return


def manage_channel_filter(filters, peer_ip):
    """
        'filter': {
            # 'prefix': '1.1.1.1/32'
            'attr': {
                'opt': '&',
                'value': [
                    {
                        'type': 'as_path'
                        'value': 4837},
                    {
                        'type': 'community'
                        'value': '4837:1239'
                    }
                ]
            }
        }
    :param filters: dict
    :param peer_ip:
    :return:
    """
    LOG.debug('try to get filter list')
    filter_list = []
    # prefix filter
    prefix = filters.get('prefix')
    if prefix:
        filter_list.append({
            'peer_ip': peer_ip,
            'type': FILTER_TYPE_PREFIX,
            'value': prefix
        })
    # attribute filter
    attr = filters.get('attr')
    if attr:
        for value in attr['value']:
            if value['type'] == FILTER_TYPE_AS_PATH:
                filter_list.append({
                    'peer_ip': peer_ip,
                    'type': FILTER_TYPE_AS_PATH,
                    'value': value['value']
                })
            elif value['type'] == FILTER_TYPE_COMMUNITY:
                filter_list.append({
                    'peer_ip': peer_ip,
                    'type': FILTER_TYPE_COMMUNITY,
                    'value': value['value']
                })
    LOG.debug('Try to update database for channel filter')
    for filter_dict in filter_list:
        update_channel_filter_in_db(filter_dict)
    return filter_list
