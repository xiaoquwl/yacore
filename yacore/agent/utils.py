# Copyright 2015 Cisco Systems, Inc.
# All rights reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

"""all interaction with yabgp agent through rest api
"""

import logging
import urllib2
import json

LOG = logging.getLogger(__name__)


def get_api_opener_v1(url, username, password):
    """
    get the http api opener with base url and username,password

    :param url: http url
    :param username: username for api auth
    :param password: password for api auth
    """
    # create a password manager
    password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()

    # Add the username and password.
    password_mgr.add_password(None, url, username, password)

    handler = urllib2.HTTPBasicAuthHandler(password_mgr)
    opener = urllib2.build_opener(handler)
    return opener


def get_data_from_agent(url, username, password, method='GET', data=None):
    """
    HTTP interaction with yabgp rest api
    :return:
    """
    # build request
    if data:
        data = json.dumps(data)
    request = urllib2.Request(url, data)
    request.add_header("Content-Type", 'application/json')
    request.get_method = lambda: method
    opener_v1 = get_api_opener_v1(url, username, password)
    try:
        res = json.loads(opener_v1.open(request).read())
        return res
    except Exception as e:
        LOG.error(e)
    return None


def search_prefix_from_agent_rib_in(bind_host, bind_port, peer_ip, username='admin', password='admin'):
    """
    :param bind_host: rest api bind host
    :param bind_port: rest api bind port
    :param peer_ip: peer ip address
    :param username: rest api username
    :param password: rest api password
    :return: prefixed dict, example: {"prefixes": [ "1.1.1.1/32", "2.2.2.2/32", "3.3.3.3/32"]} or None
    """
    url = 'http://{bind_host}:{bind_port}/v1/adj-rib-in/ipv4/{peer_ip}'
    url = url.format(bind_host=bind_host, bind_port=bind_port, peer_ip=peer_ip)
    return get_data_from_agent(url, username, password)


def get_prefix_attr_rib_in(bind_host, bind_port, peer_ip, prefix, username='admin', password='admin'):
    """
    :param bind_host: rest api bind host
    :param bind_port: rest api bind port
    :param peer_ip: peer ip address
    :param prefix: prefix string
    :param username: rest api username
    :param password: rest api password
    :return: prefixed dict, example: {
          "attr": {
            "1": 0,
            "2": [],
            "3": "10.124.1.221",
            "4": 0,
            "5": 100
          }
      } or None
    """
    url = 'http://{bind_host}:{bind_port}/v1/adj-rib-in/ipv4/{peer_ip}?prefix={prefix}'
    url = url.format(bind_host=bind_host, bind_port=bind_port, peer_ip=peer_ip, prefix=prefix)
    return get_data_from_agent(url, username, password)


def update_channel_filter(bind_host, bind_port, filter_dict, username='admin', password='admin'):
    """
    :param bind_host:
    :param bind_port:
    :param filter_dict:
    :param username:
    :param password:
    :return:
    """
    url = 'http://{bind_host}:{bind_port}/v1/channel/filter'
    url = url.format(bind_host=bind_host, bind_port=bind_port)
    return get_data_from_agent(url, username, password, method='POST', data=filter_dict)


def send_route_refresh_to_peer(bind_host, bind_port, peer_ip, username='admin', password='admin'):
    url = 'http://{bind_host}:{bind_port}/v1/peer/{peer_ip}/send/route-refresh'
    url = url.format(bind_host=bind_host, bind_port=bind_port, peer_ip=peer_ip)
    data = {
        "afi": 1,
        "safi": 1,
        "res": 0
    }
    return get_data_from_agent(url, username, password, method='POST', data=data)


def send_update_to_peer(bind_host, bind_port, peer_ip, username='admin', password='admin', data=''):
    """

    :param bind_host:
    :param bind_port:
    :param peer_ip:
    :param username:
    :param password:
    :param data:
        data
        {
            "attr":{
                "1": 0,
                "2": [],
                "3": "192.0.2.1",
                "5": 100,
                "8": ["NO_EXPORT"]
            },
          "nlri":["172.20.1.0/24","172.20.2.0/24"]
        }
    :return:
    """
    url = 'http://{bind_host}:{bind_port}/v1/peer/{peer_ip}/send/update'
    url = url.format(bind_host=bind_host, bind_port=bind_port, peer_ip=peer_ip)
    return get_data_from_agent(url, username, password, method='POST', data=data)


def get_agent_state(bind_host, bind_port, peer_ip, username='admin', password='admin'):
    url = 'http://{bind_host}:{bind_port}/v1/peer/{peer_ip}/state'
    url = url.format(bind_host=bind_host, bind_port=bind_port, peer_ip=peer_ip)
    return get_data_from_agent(url, username, password)
