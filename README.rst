yabgp core service
==================


Generate Documentation
~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    $ virtualenv yacore-virl
    $ source yacore-virl/bin/activate
    $ cd yacore/doc
    $ pip install -r requirements.txt
    $ make html

Then go to `doc/build` and open ``index.html`` in your browser.