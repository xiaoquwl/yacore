Tutorial
========

Basic Usage
~~~~~~~~~~~

We can use ``yacore`` help.

.. code:: bash

    $ yacore -h

The simple way to start a yacore process is

.. code:: bash

    $ yacored

Configuration
~~~~~~~~~~~~~

.. code-block:: js

    [DEFAULT]

    # Address to bind the API server to.
    # bind_host = 0.0.0.0

    # Port the bind the API server to.
    # bind_port = 9999

    # username and password for api server
    # username = admin
    # password = admin

    [database]

    # database configuration

    # connection url
    # eg: mongodb://10.75.44.10:27017,10.75.44.11:27017,10.75.44.12:27017
    # connection = mongodb://127.0.0.1:27017,127.0.0.1:27018,127.0.0.1:27018

    # database name
    # dbname = yabgp

    # if use replica set
    # use_replica = True

    # replica set name
    # replica_name = rs1

    # Read preference if use replica set
    # PRIMARY = 0  Queries are sent to the primary of the replica set.
    # PRIMARY_PREFERRED = 1 Queries are sent to the primary if available, otherwise a secondary.
    # SECONDARY = 2 Queries are distributed among secondaries. An error is raised if no secondaries are available.
    # SECONDARY_PREFERRED = 3  Queries are distributed among secondaries, or the primary if no secondary is available.
    # NEAREST = 4 Queries are distributed among all members.
    # read_preference = 3

    # write concern (integer or string)If this is a replica set, write operations will block until
    # they have been replicated to the specified number or tagged set of servers. w= always includes
    # the replica set primary (e.g. w=3 means write to the primary and wait until replicated to two
    # secondaries). Setting w=0 disables write acknowledgement and all other write concern options.
    # write_concern = -1

    # write concern timeout (integer) Used in conjunction with w. Specify a value in milliseconds to
    # control how long to wait for write propagation to complete. If replication does not complete
    # in the given timeframe, a timeout exception is raised.
    # write_concern_timeout = 5000


Logging and Debug
~~~~~~~~~~~~~~~~~

You check running information from log output.

.. code-block:: bash

    $ python yacored --config-file=../etc/yacore/yacore.ini
    2015-12-08 10:02:05,073.073 1127 INFO yacore.service [-] Log (Re)opened.
    2015-12-08 10:02:05,073.073 1127 INFO yacore.api.app [-] Configuration:
    2015-12-08 10:02:05,073.073 1127 INFO yacore.api.app [-] ********************************************************************************
    2015-12-08 10:02:05,073.073 1127 INFO yacore.api.app [-] Configuration options gathered from:
    2015-12-08 10:02:05,073.073 1127 INFO yacore.api.app [-] command line args: ['--config-file=../etc/yacore/yacore.ini']
    2015-12-08 10:02:05,073.073 1127 INFO yacore.api.app [-] config files: ['../etc/yacore/yacore.ini']
    2015-12-08 10:02:05,073.073 1127 INFO yacore.api.app [-] ================================================================================
    2015-12-08 10:02:05,073.073 1127 INFO yacore.api.app [-] config_dir                     = None
    2015-12-08 10:02:05,073.073 1127 INFO yacore.api.app [-] config_file                    = ['../etc/yacore/yacore.ini']
    2015-12-08 10:02:05,073.073 1127 INFO yacore.api.app [-] log_config_file                = None
    2015-12-08 10:02:05,073.073 1127 INFO yacore.api.app [-] log_dir                        = None
    2015-12-08 10:02:05,073.073 1127 INFO yacore.api.app [-] log_file                       = None
    2015-12-08 10:02:05,073.073 1127 INFO yacore.api.app [-] log_file_mode                  = 0644
    2015-12-08 10:02:05,074.074 1127 INFO yacore.api.app [-] pid_file                       = None
    2015-12-08 10:02:05,074.074 1127 INFO yacore.api.app [-] use_stderr                     = True
    2015-12-08 10:02:05,074.074 1127 INFO yacore.api.app [-] verbose                        = False
    2015-12-08 10:02:05,074.074 1127 INFO yacore.api.app [-] rest.bind_host                 = 0.0.0.0
    2015-12-08 10:02:05,074.074 1127 INFO yacore.api.app [-] rest.bind_port                 = 9901
    2015-12-08 10:02:05,074.074 1127 INFO yacore.api.app [-] rest.password                  = admin
    2015-12-08 10:02:05,074.074 1127 INFO yacore.api.app [-] rest.username                  = admin
    2015-12-08 10:02:05,074.074 1127 INFO yacore.api.app [-] database.connection            = mongodb://10.124.1.243:27017
    2015-12-08 10:02:05,074.074 1127 INFO yacore.api.app [-] database.dbname                = yabgp
    2015-12-08 10:02:05,074.074 1127 INFO yacore.api.app [-] database.use_replica           = False
    2015-12-08 10:02:05,074.074 1127 INFO yacore.api.app [-] ********************************************************************************
    2015-12-08 10:02:05,074.074 1127 INFO yacore.api.app [-] Starting build WSGI server
    2015-12-08 10:02:05,076.076 1127 INFO yacore.api.app [-] Starting server in PID 1127
    2015-12-08 10:02:05,076.076 1127 INFO yacore.api.app [-] serving on 0.0.0.0:9901, view at http://127.0.0.1:9901

