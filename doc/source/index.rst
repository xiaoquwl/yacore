.. yacore documentation master file, created by
   sphinx-quickstart on Fri May 15 10:18:44 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

YACORE Project
==============

YACORE is the core process for BGP controller. It's a REST server and provide many APIs. For more details please read
the API documentation.

Features
========

-  Get runnging and configuration information for each Agent

-  Create/Edit/Delete Route policies

-  Send BGP update/withdraw to particular agent.


Quickstarts
===========

.. toctree::
   :maxdepth: 2

   install
   tutorial
   restapi
   references

Support
=======

Please send email to penxiao@cisco.com

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`