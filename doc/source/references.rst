Reference
=========

RFCs
----

* RFC1997(BGP Communities Attribute)
* RFC2385(Protection of BGP Sessions via the TCP MD5 Signature Option)
* RFC2439(BGP Route Flap Damping)
* RFC2545(Use of BGP-4 Multiprotocol Extensions for IPv6 Inter-Domain Routing)
* RFC2858(Multiprotocol Extensions for BGP-4)
* RFC2918(Route Refresh Capability for BGP-4)
* RFC3031(Multiprotocol Label Switching Architecture)
* RFC3032(MPLS Label Stack Encoding)
* RFC3065 (Autonomous System Confederations for BGP)
* RFC3107(Carrying Label Information in BGP-4)
* RFC3392(Capabilities Advertisement with BGP-4)
* RFC4271(A Border Gateway Protocol 4)
* RFC4360(BGP Extended Communities Attribute)
* RFC4364(BGPMPLS IP Virtual Private Networks (VPNs))
* RFC4456(BGP Route Reflection An alternative to Full Mesh Internal BGP)
* RFC4724(Graceful Restart Mechanism for BGP)
* RFC4760( Multiprotocol Extensions for BGP-4)
* RFC4798(Connecting IPv6 Islands over IPv4 MPLS Using IPv6 Provider Edge Routers (6PE))
* RFC4893(BGP Support for Four-octet AS Number Space).txt
* RFC5065(Autonomous System Confederations for BGP)
* RFC5291(Outbound Route Filtering Capability for BGP-4)
* RFC5396(Textual Representation of Autonomous System (AS) Numbers).txt
* RFC5492(Capabilities Advertisement with BGP-4)
* RFC5668(4-Octet AS Specific BGP Extended Community)
* RFC5701(IPv6 Address Specific BGP Extended Community Attribute)
* RFC6368(Internal BGP as the ProviderCustomer Edge Protocol for BGPMPLS IP Virtual Private Networks (VPNs))
* RFC6472 (Recommendation for Not Using AS_SET and AS_CONFED_SET in BGP)
* RFC6513(Multicast in MPLS BGP IP VPNs)
* RFC6774(Distribution of Diverse BGP Paths)