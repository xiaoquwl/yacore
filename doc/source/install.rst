Installation
============

We recommend run ``yacore`` through python virtual-env from source
code

From source code
~~~~~~~~~~~~~~~~

Use yacore from source code:

.. code:: bash

    $ virtualenv yacore-virl
    $ source yacore-virl/bin/activate
    $ git clone https://giturl
    $ cd yacore
    $ pip install -r requirements.txt
    $ cd bin
    $ python yacored -h
